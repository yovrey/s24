let getCube = 2**3;
console.log(`The cube of 2 is ${getCube}`);

// ------------5-6

let address = ["123", "4th Street", "Barangay 5th", "6th City"];

let [streetNum, streetName, barName, city] = address;
console.log(`I live in ${streetNum}, ${streetName}, ${barName}, ${city} `);

// -----------7-8

let animal = {
	name: "Rhaegal",
	type: "dragon",
	measurement: "20 ft 3 in",
	weight: "2000 Kgs"
};

let {name, type, measurement, weight} = animal;
console.log(` ${name} is a ${type}. He weighed ${weight} with a measurement of ${measurement}. `);

// ------------9-10

let numberArr = [4, 2, 3, 5, 1];

numberArr.forEach((number) => {
	console.log(number);
})


// -----------11-12

let reduceNumber = numberArr.reduce((num1,num2) => num1 + num2);
console.log(reduceNumber);




